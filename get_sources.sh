#!/bin/bash

git clone -q https://github.com/llvm/llvm-project &
git clone -q https://github.com/llvm/test-suite &
git clone --depth=1 -q git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git linux &

git clone -q --branch=hexagon https://github.com/quic/musl &
git clone -q https://github.com/quic/qemu &

wait
wait
wait
wait
wait
