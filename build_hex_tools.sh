#!/bin/bash -x

set -euo pipefail

build_llvm_clang() {
	cd ${BASE}
	mkdir -p obj_llvm
	cd obj_llvm

	CC=clang CXX=clang++ cmake -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX:PATH=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/ \
		-DLLVM_CCACHE_BUILD:BOOL=ON \
		-DLLVM_ENABLE_LLD:BOOL=ON \
		-DLLVM_ENABLE_PROJECTS:STRING="clang;lld" \
		../llvm-project/llvm
	ninja all install
	cd ${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin
	ln -sf clang hexagon-unknown-linux-musl-clang
	ln -sf clang++ hexagon-unknown-linux-musl-clang++
}

build_clang_rt() {
	cd ${BASE}
	mkdir -p obj_clang_rt
	cd obj_clang_rt
	cmake -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DLLVM_CONFIG_PATH:PATH=../obj_llvm/bin/llvm-config \
		-DCMAKE_C_FLAGS:STRING="-G0 -fuse-ld=lld -mlong-calls --target=hexagon-unknown-linux-musl --sysroot=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/" \
		-DCMAKE_CXX_FLAGS:STRING="-G0 -fuse-ld=lld -mlong-calls --target=hexagon-unknown-linux-musl --sysroot=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/" \
		-DCMAKE_ASM_FLAGS:STRING="-G0  -mlong-calls -fno-pic --target=hexagon-unknown-linux-musl --sysroot=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/" \
		-DCMAKE_C_COMPILER:STRING="${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/hexagon-unknown-linux-musl-clang" \
		-DCMAKE_ASM_COMPILER:STRING="${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/hexagon-unknown-linux-musl-clang" \
		-DCMAKE_INSTALL_PREFIX:PATH=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/ \
		-DCMAKE_CROSSCOMPILING:BOOL=ON \
		-DCMAKE_C_COMPILER_FORCED:BOOL=ON \
		-DCMAKE_CXX_COMPILER_FORCED:BOOL=ON \
		-DCOMPILER_RT_BUILD_BUILTINS:BOOL=ON \
		-DCOMPILER_RT_BUILTINS_ENABLE_PIC:BOOL=OFF \
		-DCMAKE_SIZEOF_VOID_P=4 \
		-DCOMPILER_RT_OS_DIR= \
		-DCAN_TARGET_hexagon=1 \
		-DCAN_TARGET_x86_64=0 \
		-DCOMPILER_RT_SUPPORTED_ARCH=hexagon \
		-DLLVM_ENABLE_PROJECTS:STRING="compiler-rt" \
		../llvm-project/compiler-rt
	ninja install-compiler-rt
}

config_kernel() {
	cd ${BASE}
	mkdir -p obj_linux
	cd linux
	make O=../obj_linux ARCH=hexagon \
		KBUILD_CFLAGS_KERNEL="-mlong-calls" \
	       	CC=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/hexagon-unknown-linux-musl-clang \
	       	LD=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/ld.lld \
		KBUILD_VERBOSE=1 comet_defconfig
}
build_kernel() {
	${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/hexagon-unknown-linux-musl-clang --version
	cd ${BASE}
	cd obj_linux
	make -j $(nproc) \
		KBUILD_CFLAGS_KERNEL="-mlong-calls" \
      		ARCH=hexagon \
		KBUILD_VERBOSE=1 comet_defconfig \
		V=1 \
	       	CC=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/hexagon-unknown-linux-musl-clang \
	       	AS=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/hexagon-unknown-linux-musl-clang \
	       	LD=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/ld.lld \
	       	OBJCOPY=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/llvm-objcopy \
	       	OBJDUMP=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/llvm-objdump \
	       	LIBGCC=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/lib/libclang_rt.builtins-hexagon.a \
		vmlinux
}
build_kernel_headers() {
	cd ${BASE}
	cd obj_linux
	make -j $(nproc) \
	        ARCH=hexagon \
	       	CC=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/clang \
		TOOLCHAIN_INSTALL_HDR_PATH=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl \
		headers_install
}

build_musl_headers() {
	cd ${BASE}
	cd musl
	make clean
	CC=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/hexagon-unknown-linux-musl-clang \
		CROSS_COMPILE=hexagon-unknown-linux-musl \
	       	LDFLAGS="-fuse-ld=lld" \
	       	LIBCC=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/lib/libclang_rt.builtins-hexagon.a \
		CFLAGS="-G0 -O0 -mv65 -fno-builtin -fno-rounding-math --target=hexagon-unknown-linux-musl" \
		./configure --target=hexagon --prefix=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl
	PATH=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/:$PATH make CROSS_COMPILE= install-headers
}

build_musl() {
	cd ${BASE}
	cd musl
	make clean
	CROSS_COMPILE=hexagon-unknown-linux-musl CC=clang LDFLAGS="-fuse-ld=lld" LIBCC=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/lib/libclang_rt.builtins-hexagon.a \
		CFLAGS="-G0 -O0 -mv65 -fno-builtin -fno-rounding-math --target=hexagon-unknown-linux-musl" \
		./configure --target=hexagon --prefix=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl
	PATH=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/:$PATH make -j CROSS_COMPILE= install
	cd ${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/lib
	ln -sf libc.so ld-musl-hexagon.so
	ln -sf ld-musl-hexagon.so ld-musl-hexagon.so.1
}

build_libcxx() {
	cd ${BASE}
	mkdir -p obj_libcxx
	cd obj_libcxx
	cmake -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DLLVM_CONFIG_PATH:PATH=../obj_llvm/bin/llvm-config \
		-DCMAKE_C_FLAGS:STRING="-G0 -fuse-ld=lld --target=hexagon-unknown-linux-musl --sysroot=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/" \
		-DCMAKE_CXX_FLAGS:STRING="-G0 -fuse-ld=lld --target=hexagon-unknown-linux-musl --sysroot=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/" \
		-DCMAKE_C_COMPILER:STRING="${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/hexagon-unknown-linux-musl-clang" \
		-DCMAKE_CXX_COMPILER:STRING="${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/hexagon-unknown-linux-musl-clang++" \
		-DCMAKE_INSTALL_PREFIX:PATH=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/ \
		-DCMAKE_CROSSCOMPILING:BOOL=ON \
		-DLLVM_ENABLE_PROJECTS:STRING="libcxx" \
		../llvm-project/libcxx
	ninja check-cxx || /bin/true
	ninja install-cxx
}

build_libunwind() {
	cd ${BASE}
	mkdir -p obj_libunwind
	cd obj_libunwind
	cmake -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DLLVM_CONFIG_PATH:PATH=../obj_llvm/bin/llvm-config \
		-DCMAKE_C_FLAGS:STRING="-G0 -fuse-ld=lld --target=hexagon-unknown-linux-musl --sysroot=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/" \
		-DCMAKE_CXX_FLAGS:STRING="-G0 -fuse-ld=lld --target=hexagon-unknown-linux-musl --sysroot=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/" \
		-DCMAKE_C_COMPILER:STRING="${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/hexagon-unknown-linux-musl-clang" \
		-DCMAKE_CXX_COMPILER:STRING="${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/hexagon-unknown-linux-musl-clang++" \
		-DCMAKE_INSTALL_PREFIX:PATH=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/ \
		-DCMAKE_CROSSCOMPILING:BOOL=ON \
		-DLLVM_ENABLE_PROJECTS:STRING="libunwind" \
		../llvm-project/libunwind
	ninja install-unwind
}

build_libcxxabi() {
	cd ${BASE}
	mkdir -p obj_libcxxabi
	cd obj_libcxxabi
	cmake -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DLLVM_CONFIG_PATH:PATH=../obj_llvm/bin/llvm-config \
		-DCMAKE_C_FLAGS:STRING="-G0 -fuse-ld=lld --target=hexagon-unknown-linux-musl --sysroot=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/" \
		-DCMAKE_CXX_FLAGS:STRING="-G0 -fuse-ld=lld --target=hexagon-unknown-linux-musl --sysroot=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/" \
		-DCMAKE_C_COMPILER:STRING="${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/hexagon-unknown-linux-musl-clang" \
		-DCMAKE_CXX_COMPILER:STRING="${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/hexagon-unknown-linux-musl-clang++" \
		-DCMAKE_INSTALL_PREFIX:PATH=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/ \
		-DCMAKE_CROSSCOMPILING:BOOL=ON \
		-DLLVM_ENABLE_PROJECTS:STRING="libcxxabi" \
		../llvm-project/libcxxabi
	ninja check-cxxabi || /bin/true
	ninja install-cxxabi
}

build_qemu() {
	cd ${BASE}
	mkdir -p obj_qemu
	cd obj_qemu
	../qemu/configure --disable-fdt --disable-capstone --disable-guest-agent \
		--target-list=hexagon-linux-user --prefix=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu \

#	--cc=clang \
#	--cross-prefix=hexagon-unknown-linux-musl-
#	--cross-cc-hexagon="hexagon-unknown-linux-musl-clang" \
#		--cross-cc-cflags-hexagon="-mv67 --sysroot=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl"

	make -j
	PATH=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin:$PATH \
		QEMU_LD_PREFIX=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl \
		make check-tcg CROSS_CC_GUEST=hexagon-unknown-linux-musl-clang V=1 --keep-going || /bin/true
	make -j install
}

test_llvm() {
	cd ${BASE}
	mkdir -p obj_test-suite
	cd obj_test-suite
	cmake -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-C../test-suite/cmake/caches/O3.cmake \
		-DTEST_SUITE_EXTRA_C_FLAGS:STRING="-G0 -fuse-ld=lld --target=hexagon-unknown-linux-musl --sysroot=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/" \
		-DTEST_SUITE_EXTRA_CXX_FLAGS:STRING="-G0 -fuse-ld=lld --target=hexagon-unknown-linux-musl --sysroot=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/" \
		-DTEST_SUITE_EXTRA_EXE_LINKER_FLAGS:STRING="-G0 -fuse-ld=lld --target=hexagon-unknown-linux-musl --sysroot=${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/target/hexagon-unknown-linux-musl/" \
		-DTEST_SUITE_CXX_ABI:STRING=libc++abi \
		-DTEST_SUITE_RUN_UNDER:STRING="${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/qemu-hexagon" \
		-DTEST_SUITE_RUN_BENCHMARKS:BOOL=OFF \
		-DTEST_SUITE_LIT_FLAGS:STRING="-v" \
		-DBENCHMARK_USE_LIBCXX:BOOL=ON \
		-DCMAKE_C_COMPILER:STRING="${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/hexagon-unknown-linux-musl-clang" \
		-DCMAKE_CXX_COMPILER:STRING="${TOOLCHAIN_INSTALL}/x86_64-linux-gnu/bin/hexagon-unknown-linux-musl-clang++" \
		-DSMALL_PROBLEM_SIZE:BOOL=ON \
		../test-suite
	ninja
	ninja check || /bin/true
}
BASE=${PWD}
build_llvm_clang
config_kernel
build_kernel_headers
build_musl_headers
#mklinks
build_clang_rt
build_musl
#build_libunwind
#build_libcxxabi
#build_libcxx
build_qemu
# Needs c++:
#test_llvm
build_kernel
echo done
